@extends('layouts.default')
@section('content')

    <div class="box box-primary" >
    <form role="form" method="POST" action="{{ url('/create-profile') }}" enctype="multipart/form-data">

        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
            <label for="first_name">First Name</label>
    <input class="form-control" type="text"  placeholder="First Name" name="first_name" id="first_name" required>
</div>

        <div class="form-group">
            <label for="last_name">Last Name</label>
            <input class="form-control" type="text"  placeholder="Last Name" name="last_name" id="last_name">
        </div>
        <div class="form-group">
            <label for="last_name">Date of Birth</label>
            <input class="form-control" type="text"  placeholder="Date of Birth" name="dob" id="dob">
        </div>
        <div class="form-group">
            <label for="gender">Gender</label>
        <select class="form-control"  name="gender" id="gender">
            <option>Select Gender...</option>
                  <option>Male</option>
                    <option>Female</option>
            <option>Other</option>

        </select>
            </div>
        <div class="form-group">
            <label for="phone_no">Phone No</label>
            <input class="form-control" type="text"  placeholder="Phone No" name="phone_no" id="phone_no">
        </div>
        <div class="form-group">
            <label for="profile_description">Profile Description</label>
            <textarea name="profile_description" id="profile_description" class="form-control" rows="3" placeholder="Profile Description ..."></textarea>
        </div>
        <div class="form-group">
            <label  for="country">Country</label>
        <select class="form-control"  name="country" id="country" required>
            <option>Select Country...</option>
            @foreach($country as $countries)
                <option value="{{$countries->id}}">{{$countries->country}}</option>
            @endforeach
        </select>

            </div>
        <div class="form-group">
            <label for="nationality">Nationality</label>
        <select class="form-control"  name="nationality" id="nationality" required>
            <option>Select Nationality...</option>
            @foreach($country as $countries)
                <option value="{{$countries->id}}">{{$countries->nationality}}</option>
            @endforeach
        </select>
            </div>

        <div class="form-group">
            <label for="field_position">Field Position</label>
        <select class="form-control" name="field_position" id="field_position" required>
            <option>Select Field Position...</option>
            @foreach($fieldPosition as $fieldPositions)
            <option value="{{$fieldPositions->id}}">{{$fieldPositions->description}}</option>
            @endforeach
        </select>
            </div>
        <div class="form-group">
            <label for="leg">Leg</label>
            <input class="form-control" type="text"  placeholder="Leg" name="leg" id="leg">
        </div>
        <div class="form-group">
            <label for="current_club">Current Club</label>
            <input class="form-control" type="text"  placeholder="Current Club" name="current_club" id="current_club">
        </div>
        <div class="form-group">
            <label for="video_link">Video Link</label>
            <input class="form-control" type="text"  placeholder="Video Link" name="video_link" id="video_link">
        </div>

        <div class="form-group">
            <label for="photo">Upload Photo</label>
            <input id="photo" type="file" name="photo" placeholder="Upload Photo">
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Save</button>
           &nbsp;&nbsp;&nbsp;
            <button type="clear" class="btn btn-primary">Clear</button>
        </div>
    </form>
        <br clear="all">
        <br clear="all">
    </div>

@endsection