@extends('layouts.default')
@section('content')
<div class="row">
    <div class="col-md-3">

        <div class="user-wrapper">
            <img src="{{$profilePic}}" class="img-responsive">
            <div class="description">
                <h4 class="set-clr"><strong>{{$userProfile->first_name." ".$userProfile->last_name}} </strong></h4>
                <h5><strong>{{$field_position}} </strong></h5>

            </div>
        </div>
        <!--USER WRAPPER SECTION END-->
    </div>
    <!--LEFT SIDE SECTION END-->
    <div class="col-md-9  user-wrapper">
        <div class="description">
            <div class="row">
                <div class="col-md-12">
                    <h3>Profile Description: </h3>
                    <hr>
                    <p>
                        {{$userProfile->description}}
                    </p>
                </div>
            </div>
            <!--WHO AM I SECTION END-->
            <div class="row">
                <div class="col-md-12">
                    <h3>General Information: </h3>
                    <hr>
                    <strong>Date of Birth.</strong>{{$userProfile->date_of_birth}}
                    <br>
                    <hr>
                    <strong>Gender</strong>{{$userProfile->gender}}
                    <br>
                    <hr>
                    <strong>Country</strong>{{$country}}
                    <br>
                    <hr>
                    <strong>Nationality</strong>{{$nationality}}
                    <br>
                    <hr>
                    <strong>Email: </strong>{{Auth::User()->email}}
                    <br>
                    <hr>
                    <strong>Phone No.</strong>{{$userProfile->phone_no}}
                </div>
            </div>
            <!--EDUCATION SECTION END-->
            <div class="row">
                <div class="col-md-12">
                    <h3>Soccer Information : </h3>
                    <hr>
                    <strong>Current Club: </strong>{{$userProfile->current_club}}
                    <br>
                    <hr>
                    <strong>Video Link: </strong>{{$userProfile->video_link}}
                    <br>
                    <hr>
                    <strong>Leg: </strong>{{$userProfile->leg}}
                    <br>
                    <hr>

                </div>
            </div>

        </div>
    </div>
    <!--RIGHT SIDE SECTION END-->
</div>
@endsection