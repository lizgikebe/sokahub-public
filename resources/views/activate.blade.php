<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Soka Hub | Activation</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->

    <link href="{{URL::asset("/css/bootstrap.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{URL::asset("/dist/css/AdminLTE.min.css")}}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body class="lockscreen">
<!-- Automatic element centering -->
<div class="lockscreen-wrapper">
    <div class="lockscreen-logo">
        <a href="../../index2.html"><b>{{Auth::user()->name}}</b></a>
    </div>
    <!-- User name -->
    <div class="lockscreen-name">Thank you for registering <br> Check your email to verify and access SokaHub
        <br><br></div>

    <!-- START LOCK SCREEN ITEM -->
    <div class="lockscreen-item">
        <!-- lockscreen image -->
        <div class="lockscreen-image">
            <img src="{{URL::asset("/dist/img/soccer.jpg")}}" alt="{{Auth::user()->name}}"/>
        </div>
        <!-- /.lockscreen-image -->

        <!-- lockscreen credentials (contains the form) -->
        <form class="lockscreen-credentials">
            <div class="input-group">
                <input type="password" class="form-control" placeholder="Activation Code" />
                <div class="input-group-btn">
                    <button class="btn"><i class="fa fa-arrow-right text-muted"></i></button>
                </div>
            </div>
        </form><!-- /.lockscreen credentials -->

    </div><!-- /.lockscreen-item -->
    <div class="help-block text-center">
        Or Enter the activation code sent on email
    </div>

    <div class='lockscreen-footer text-center'>
        Copyright &copy; 2014-2015 <b><a href="http://sokahub.com" class='text-black'>Sokahub</a></b><br>
        All rights reserved
    </div>
</div><!-- /.center -->

<!-- jQuery 2.1.3 -->

<script src="{{URL::asset("/plugins/jQuery/jQuery-2.1.3.min.js")}}"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="{{URL::asset("/bootstrap/js/bootstrap.min.js")}}" type="text/javascript"></script>
</body>
</html>