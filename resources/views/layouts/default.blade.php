<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Sokahub</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="{{URL::asset("/default/css/bootstrap.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- FontAwesome 4.3.0 -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.0 -->
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{URL::asset("/default/dist/css/AdminLTE.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link href="{{URL::asset("/default/dist/css/skins/_all-skins.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="{{URL::asset("/default/plugins/iCheck/flat/blue.css")}}" rel="stylesheet" type="text/css" />
    <!-- Morris chart -->
    <link href="{{URL::asset("/default/plugins/morris/morris.css")}}" rel="stylesheet" type="text/css" />
    <!-- jvectormap -->
    <link href="{{URL::asset("/default/plugins/jvectormap/jquery-jvectormap-1.2.2.css")}}" rel="stylesheet" type="text/css" />
    <!-- Date Picker -->
    <link href="{{URL::asset("/default/plugins/datepicker/datepicker3.css")}}" rel="stylesheet" type="text/css" />
    <!-- Daterange picker -->
    <link href="{{URL::asset("/default/plugins/daterangepicker/daterangepicker-bs3.css")}}" rel="stylesheet" type="text/css" />
    <!-- bootstrap wysihtml5 - text editor -->
    <link href="{{URL::asset("/default/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css")}}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body class="skin-blue">
<div class="wrapper" >

    <header class="main-header" >
        <!-- Logo -->
        <a href="/" class="logo"><b>SokaHub</b></a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">

                <ul class="nav navbar-nav">
                    <!-- Messages: style can be found in dropdown.less-->

                    <li class="dropdown messages-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-envelope-o"></i>
                            <span class="label label-success">{{$countMessage}}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">You have {{$countMessage}} messages</li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                <ul class="menu" style="height: auto;">
                                    @foreach($userMessage as $userMessages)
                                    <li><!-- start message -->
                                        <a href="#">
                                            <div class="pull-left">
                                                <img src="{{URL::asset($profilePic)}}" class="img-circle" alt="User Image"/>
                                            </div>
                                            <h4>
                                                {{$userMessages->subject}}
                                                <small><i class="fa fa-clock-o"></i> {{$userMessages->created_at}} </small>
                                            </h4>
                                            <p>{{$userMessages->message}}</p>
                                        </a>
                                    </li><!-- end message -->
                                    @endforeach
                                </ul>
                            </li>
                            <li class="footer"><a href="/messages">See All Messages</a></li>

                        </ul>

                      </li>

                    <!-- Notifications: style can be found in dropdown.less -->
                    <li class="dropdown notifications-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-bell-o"></i>
                            <span class="label label-warning">{{ $userNotificationCount }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">You have {{ $userNotificationCount }} notifications</li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                <ul class="menu">
                                    @foreach( $userNotification as $userNotifications )
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-users text-aqua"></i> {{$userNotifications->notification}}
                                        </a>
                                    </li>
                                    @endforeach

                                </ul>
                            </li>
                            <li class="footer"><a href="#">View all</a></li>
                        </ul>
                    </li>
                    <!-- Tasks: style can be found in dropdown.less -->
                    <li class="dropdown tasks-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-flag-o"></i>
                            <span class="label label-danger">{{$userEventsCount}}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">You have {{$userEventsCount}} new events</li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                <ul class="menu">
                                    @foreach($userEvents as $userEvent)
                                    <li><!-- Task item -->
                                        <a href="#">
                                            <h3>
                                                Event: {{$userEvent->title }}
                                                <br><br>
                                                Date: {{$userEvent->date }}
                                                <br><br>
                                                Venue: {{$userEvent->venue }}
                                            </h3>

                                        </a>
                                    </li><!-- end task item -->
                                    @endforeach
                                </ul>
                            </li>
                            <li class="footer">
                                <a href="#">View all events</a>
                            </li>
                        </ul>
                    </li>
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="{{URL::asset($profilePic)}}" class="user-image" alt="{{Auth::User()->name}}"/>
                            <span class="hidden-xs">{{Auth::User()->name}}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="{{URL::asset($profilePic)}}" class="img-circle" alt="{{Auth::User()->name}}" />
                                <p>
                                    {{Auth::User()->name}} - {{$fieldPositions}}
                                    <small>Member since {{$createdAt}}</small>
                                </p>
                            </li>

                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="{{$profileUrl}}" class="btn btn-default btn-flat">{{$profileName}}</a>
                                </div>
                                <div class="pull-right">
                                    <a href="/auth/logout" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{URL::asset($profilePic)}}" class="img-circle" alt="{{Auth::User()->name}}" />
                </div>
                <div class="pull-left info">
                    <p>{{Auth::User()->name}}</p>

                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <!-- search form -->
            <form action="#" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
                </div>
            </form>
            <!-- /.search form -->
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="header">MAIN NAVIGATION</li>

                <li>
                    <a href="/messages">
                        <i class="fa fa-envelope"></i>
                        <span>Messages</span>
                        <span class="label label-primary pull-right">{{$countMessage}}</span>
                    </a>
                </li>
                <li>
                    <a href="/latest-news">
                        <i class="fa fa-files-o"></i>
                        <span>News</span>
                        <span class="label label-primary pull-right">{{$countNews}}</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-th"></i> <span>Events</span>
                        <small class="label pull-right bg-green">{{$userEventsCount}}</small>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-laptop"></i>
                        <span>Games and Scores</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-edit"></i>
                        <span>Soccer Rules</span>
                    </a>
                </li>

                <li>
                    <a href="#">
                        <i class="fa fa-table"></i>
                        <span>Groups</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-calendar"></i>
                        <span>Calendar</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-folder"></i>
                        <span>Clubs</span>
                    </a>

                </li>
                  </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
               {{$title}}
                <small></small>
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-8">
                    @yield('content')
                </div>
                <!--Wlcome to sokahub. Content starts here -->
                <div class="col-md-4" align="right" >
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Games</h3>
                            <div class="box-tools">
                                <ul class="pagination pagination-sm no-margin pull-right">
                                    <li><a href="#">«</a></li>
                                    <li><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">»</a></li>
                                </ul>
                            </div>
                        </div><!-- /.box-header -->
                        <div class="box-body no-padding">
                            <table class="table">
                                <tbody><tr>

                                    <th>Game</th>
                                    <th>Date</th>
                                    <th>Time</th>

                                </tr>
                                @foreach($gameRec as $gameRecs)
                                    <tr>
                                        <td>{!! $gameRecs->game.' VS '. $gameRecs->opponent !!}</td>
                                        <td>
                                            {!! $gameRecs->date !!}
                                        </td>
                                        <td>
                                            {!! $gameRecs->time !!}
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody></table>
                            <div class="box-footer
                             text-center">
                                <a href="javascript:;" class="uppercase">More Games</a>
                            </div>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->



                </div>

            </div>

        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 1.0
        </div>
        <strong>Copyright © 2014-2015 <a href="http://sokahub.com">Soka Hub</a>.</strong> All rights reserved.
    </footer>
</div><!-- ./wrapper -->

<!-- jQuery 2.1.3 -->
<script src="{{URL::asset("/default/plugins/jQuery/jQuery-2.1.3.min.js")}}"></script>
<!-- jQuery UI 1.11.2 -->
<script src="http://code.jquery.com/ui/1.11.2/jquery-ui.min.js" type="text/javascript"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.2 JS -->
<script src="{{URL::asset("/default/js/bootstrap.min.js")}}" type="text/javascript"></script>
<!-- Morris.js charts -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="{{URL::asset("/default/plugins/morris/morris.min.js")}}" type="text/javascript"></script>
<!-- Sparkline -->
<script src="{{URL::asset("/default/plugins/sparkline/jquery.sparkline.min.js")}}" type="text/javascript"></script>
<!-- jvectormap -->
<script src="{{URL::asset("/default/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js")}}" type="text/javascript"></script>
<script src="{{URL::asset("/default/plugins/jvectormap/jquery-jvectormap-world-mill-en.js")}}" type="text/javascript"></script>
<!-- jQuery Knob Chart -->
<script src="{{URL::asset("/default/plugins/knob/jquery.knob.js")}}" type="text/javascript"></script>
<!-- daterangepicker -->
<script src="{{URL::asset("/default/plugins/daterangepicker/daterangepicker.js")}}" type="text/javascript"></script>
<!-- datepicker -->
<script src="{{URL::asset("/default/plugins/datepicker/bootstrap-datepicker.js")}}" type="text/javascript"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{URL::asset("/default/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js")}}" type="text/javascript"></script>
<!-- iCheck -->
<script src="{{URL::asset("/default/plugins/iCheck/icheck.min.js")}}" type="text/javascript"></script>
<!-- Slimscroll -->
<script src="{{URL::asset("/default/plugins/slimScroll/jquery.slimscroll.min.js")}}" type="text/javascript"></script>
<!-- FastClick -->
<script src="{{URL::asset("/default/plugins/fastclick/fastclick.min.js")}}"></script>
<!-- AdminLTE App -->
<script src="{{URL::asset("/default/dist/js/app.min.js")}}" type="text/javascript"></script>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{URL::asset("/default/dist/js/pages/dashboard.js")}}" type="text/javascript"></script>

<!-- AdminLTE for demo purposes -->
<script src="{{URL::asset("/default/dist/js/demo.js")}}" type="text/javascript"></script>
</body>
</html>