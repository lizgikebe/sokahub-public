@extends($layout)
@section('content')
    <div class="box box-success">
        <div style="position: relative; overflow: hidden; width: auto; height: auto;" class="slimScrollDiv">
            <div style="overflow: hidden; width: auto; height: auto;" class="box-body chat" id="chat-box">
                <!-- chat item -->
                @foreach($newsRec as $NewsRec)
                    <div class="item">
                        @if($NewsRec->image_url=='')
                            <img src="dist/img/user4-128x128.jpg" alt="user image" class="online">
                        @endif
                        @if($NewsRec->image_url!='')
                            <img src="{{$NewsRec->image_url}}" alt="user image" class="online">
                        @endif
                        <p class="message">
                            <a href="#" class="name">
                                <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> <?php
                                    $unix = strtotime($NewsRec->created_at);  //puts the mysql datetime into unix timestamp this probably can be done within mysql also
                                    $datetime = date('F j Y h:ia',$unix);
                                        echo $datetime;
                                    ?></small>
                                {{$NewsRec->title}}
                            </a>
                            {{$NewsRec->news}}
                        </p>

                    </div ><!-- /.item -->
                    <hr>
                @endforeach
                <hr>

            </div><div style="background: none repeat scroll 0% 0% rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 187.126px;" class="slimScrollBar"></div>
            <div style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: none repeat scroll 0% 0% rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;" class="slimScrollRail"></div></div><!-- /.chat -->

    </div>
@endsection