@extends('layouts.registerlogin')

@section('content')

    <div id="loginModal" class="modal show" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    Welcome to SokaHub: Where we link talented Soccer players with clubs and give you up to date
                    soccer news from around the globe.

                    <h1 class="text-center">Login to SokaHub</h1>
                </div>
                <div class="modal-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                                <!--    <form class="form col-md-12 center-block">-->
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <input type="email" name="email" value="{{ old('email') }}"class="form-control input-lg" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <input type="password"  name="password" class="form-control input-lg" placeholder="Password" id="password">
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="remember"> Remember Me
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary btn-lg btn-block">Sign In</button>
                                <span class="pull-right"><a href="{{ url('/auth/register') }}">Register</a></span><span><a href="#">Need help?</a>
                                <a class="btn btn-link" href="{{ url('/password/email') }}">Forgot Your Password?</a></span>

                            </div>
                        </form>


                        <!--   </form>-->
                </div>

            </div>
        </div>
    </div>

@endsection
