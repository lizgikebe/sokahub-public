@extends($layout)
@section('content')
        <div style="position: relative; overflow: hidden; width: auto; height: auto;" class="slimScrollDiv">
            <div style="overflow: hidden; width: auto; height: auto;" class="box-body chat" id="chat-box">
                <!-- chat item -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{$newsRec->title}}</h3>

                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <p>{{$newsRec->news}}</p>
                       </div><!-- /.box-body -->
                </div>
            </div><div style="background: none repeat scroll 0% 0% rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 187.126px;" class="slimScrollBar"></div><div style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: none repeat scroll 0% 0% rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;" class="slimScrollRail"></div></div><!-- /.chat -->


    @endsection