@extends('layouts.default')

@section('content')


        <section class="content">
            <div class="row">
                <div class="col-md-9" style="width: 100%">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <a href="compose.html" type="button" class="btn btn-info">Compose</a>
                            <div class="box-tools pull-right">
                                <div class="has-feedback">
                                    <input class="form-control input-sm" placeholder="Search Mail" type="text">
                                    <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                </div>
                            </div><!-- /.box-tools -->
                        </div><!-- /.box-header -->
                        <div class="box-body no-padding">

                            <div class="table-responsive mailbox-messages">
                                <table class="table table-hover table-striped">
                                    <tbody>
                                    @foreach($Messages as $Message)

                                    <tr>
                                        <td><div aria-disabled="false" aria-checked="false" style="position: relative;" class="icheckbox_flat-blue"><input style="position: absolute; opacity: 0;" type="checkbox"><ins style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: none repeat scroll 0% 0% rgb(255, 255, 255); border: 0px none; opacity: 0;" class="iCheck-helper"></ins></div></td>
                                        <td class="mailbox-star"><a href="/message/{{$Message->id}}"><i class="fa fa-star text-yellow"></i></a></td>
                                        <td class="mailbox-name"><a href="/message/{{$Message->id}}">
                                                {{$Message->from->profile->first_name." ".$Message->from->profile->other_names}}</a></td>
                                        <td class="mailbox-subject"><b> {{$Message->subject}}</b> - {{$Message->message}}...</td>
                                        <td class="mailbox-attachment"></td>
                                        <td class="mailbox-date">  {{$Message->created_at}}</td>
                                    </tr>
                                    @endforeach

                                    </tbody>
                                </table><!-- /.table -->
                            </div><!-- /.mail-box-messages -->
                        </div><!-- /.box-body -->
                        <div class="box-footer no-padding">
                            <div class="mailbox-controls">
                                <!-- Check all button -->
                                <button class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i></button>
                                <div class="btn-group">
                                    <button class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
                                    <button class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
                                    <button class="btn btn-default btn-sm"><i class="fa fa-share"></i></button>
                                </div><!-- /.btn-group -->
                                <button class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>
                                <div class="pull-right">
                                    1-50/200
                                    <div class="btn-group">
                                        <button class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                                        <button class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
                                    </div><!-- /.btn-group -->
                                </div><!-- /.pull-right -->
                            </div>
                        </div>
                    </div><!-- /. box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </section>



@endsection