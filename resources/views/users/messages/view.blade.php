@extends('layouts.default')

@section('content')

    <div class="box box-success">
        <div style="position: relative; overflow: hidden; width: auto; height: auto;" class="slimScrollDiv">
            <div style="overflow: hidden; width: auto; height: auto;" class="box-body chat" id="chat-box">
                <!-- chat item -->


                    <div class="item">
                        <img src="{{URL::asset ('/images/profile/'.$messageDetails->photo)}}" alt="user image" class="online">

                        <p class="message">
                                <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 2:15</small>
                               <strong> {{$messagesRec->subject}}</strong>
                            <br>
                        <hr>
                            {{$messagesRec->message}}
                        </p>

                        @if($messagesRec->is_parent==1)

                            <hr>
                            @foreach($children as $childrens)
                            <p class="message">
                                <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 2:15</small>
                                {{$childrens->message}}
                            </p>
                            <hr>
                            @endforeach
                            @endif


                    </div><!-- /.item -->


            </div><div style="background: none repeat scroll 0% 0% rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 187.126px;" class="slimScrollBar"></div><div style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: none repeat scroll 0% 0% rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;" class="slimScrollRail"></div></div><!-- /.chat -->

    </div>


@endsection