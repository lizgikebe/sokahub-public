@extends($layout)
@section('content')
    <div class="box box-success">
        <div style="position: relative; overflow: hidden; width: auto; height: auto;" class="slimScrollDiv">
            <div style="overflow: hidden; width: auto; height: auto;" class="box-body chat" id="chat-box">
                <div class="item">

                <form role="form" method="POST" action="{{ url('/post-news') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label>News Type</label>
                        <select class="form-control" id="type" name="type">
                            <option>Select...</option>
                        @foreach($news_type as $news_types)
                            <option value="{{ $news_types->id}}">{{ $news_types->descriprion}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Title</label>
                        <input class="form-control" placeholder="Enter Title" type="text" id="title" name="title">
                    </div>
                <div class="form-group">
                    <label>Enter News</label>
                    <textarea class="form-control" rows="3" id="news" name="news" placeholder="Enter ..."></textarea>
                </div>
                <table>
                    <tr>
                        <td>
                            <div class="form-group">
                                <label for="uploadfile">Upload File</label>
                                <input id="photo" type="file" name="photo" placeholder="Upload Photo">
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <div class="box-footer">
                                    <button type="submit" id="submitnews" name="submitnews" class="btn btn-primary">
                                        Save</button>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
                    </form>
                    </div>
                <hr>
                           </div>
            <div style="background: none repeat scroll 0% 0% rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 187.126px;" class="slimScrollBar"></div><div style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: none repeat scroll 0% 0% rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;" class="slimScrollRail"></div></div><!-- /.chat -->

    </div>

    @endsection