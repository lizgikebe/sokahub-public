@extends($layout)
@section('content')
    <div class="box box-success">
        <div style="position: relative; overflow: hidden; width: auto; height: auto;" class="slimScrollDiv">
            <div style="overflow: hidden; width: auto; height: auto;" class="box-body chat" id="chat-box">
                <div class="item">

                <form role="form" method="POST" action="{{ url('/post-games') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group">
                        <label>Team</label>
                        <input class="form-control" placeholder="Enter Team" type="text" id="team" name="team">
                    </div>
                    <div class="form-group">
                        <label>Oponent</label>
                        <input class="form-control" placeholder="Enter Oponent" type="text" id="oponent" name="oponent">
                    </div>
                    <div class="form-group">
                        <label>Venue</label>
                        <input class="form-control" placeholder="Enter Venue" type="text" id="venue" name="venue">
                    </div>
                    <div class="form-group">
                        <label>Date</label>
                        <input class="form-control" placeholder="Enter Date" type="date" id="date" name="date">
                    </div>
                    <div class="form-group">
                        <label>Time</label>
                        <input class="form-control" placeholder="Enter Time" type="time" id="time" name="time">
                    </div>
                    <div class="form-group">
                        <label>Reference</label>
                        <input class="form-control" placeholder="Enter Reference" type="text" id="reference" name="reference">
                    </div>

                <table>
                    <tr>
                        <td>
                            <div class="form-group">
                                <label for="uploadfile">Upload File</label>
                                <input id="photo" type="file" name="photo" placeholder="Upload Photo">
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <div class="box-footer">
                                    <button type="submit" id="submitnews" name="submitnews" class="btn btn-primary">
                                        Save</button>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
                    </form>
                    </div>
                <hr>
                           </div>
            <div style="background: none repeat scroll 0% 0% rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 187.126px;" class="slimScrollBar"></div><div style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: none repeat scroll 0% 0% rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;" class="slimScrollRail"></div></div><!-- /.chat -->

    </div>

    @endsection