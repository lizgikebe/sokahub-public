<?php namespace App\Providers;


use App\Model\FieldPosition;
use Illuminate\Support\ServiceProvider;
use App\Model\News;
use App\Model\Game;
use App\Model\Message;
use App\Model\Notification;
use App\Model\Profile;
use App\Model\Event;
use Auth;

class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{

        view()->composer('layouts.clientlayout', function ($view) {
            $profilePic = '/images/profile/669468.jpg';
            $fieldPositions = "";
            $userMessage = Message::where('user_id', '=', (Auth::user()->id))->get();
            $countMessage = count($userMessage);
            $userNotification = Notification::where('user_id', '=', (Auth::user()->id))->where('opened', '=', 0)->get();
            $userNotificationCount = count($userNotification);
            $date = date('Y-m-d H:i:s');
            $userEvents = Event::where('date', '>=', $date)->get();
            $userEventsCount = count($userEvents);
            $userProfile = Profile::where('user_id', '=', (Auth::user()->id))->first();
            //  dd($profilePic);
            if (count($userProfile) > 0) {
                $fieldPositionsRec = FieldPosition::where('id', '=', $userProfile->field_position)->first();
                $fieldPositions = $fieldPositionsRec->description;
                $profilePic = '/images/profile/' . $userProfile->photo;

            }
            $createdAt = date('Y-m-d', strtotime(Auth::user()->created_at));
            //$createdAt=Auth::user()->created_at;
            $getnews = News::where('new', '=', 1)->get();
            $countNews = count($getnews);
            $gameRec = Game::orderBy('id', 'desc')->take(15)->get();

            $view->with(array('userMessage' => $userMessage,
                'countMessage' => $countMessage,
                'userNotification' => $userNotification,
                'userNotificationCount' => $userNotificationCount,
                'userEvents' => $userEvents,
                'userEventsCount' => $userEventsCount,
                'userProfile' => $userProfile,
                'fieldPositions' => $fieldPositions,
                'createdAt' => $createdAt,
                'getnews' => $getnews,
                'countNews' => $countNews,
                'gameRec' => $gameRec,
                'profilePic' => $profilePic
            ));
        });

                view()->composer('layouts.default', function ($view) {
                    $profilePic = '/images/profile/669468.jpg';
                    $fieldPositions = "";
                    $userMessage = Message::where('user_id', '=', (Auth::user()->id))->get();
                    $countMessage = count($userMessage);
                    $userNotification = Notification::where('user_id', '=', (Auth::user()->id))->where('opened', '=', 0)->get();
                    $userNotificationCount = count($userNotification);
                    $date = date('Y-m-d H:i:s');
                    $userEvents = Event::where('date', '>=', $date)->get();
                    $userEventsCount = count($userEvents);
                    $userProfile = Profile::where('user_id', '=', (Auth::user()->id))->first();
                    $profileName="Create Profile";
                    $profileUrl="/create-profile";
                    //  dd($profilePic);
                    if (count($userProfile) > 0) {
                        $fieldPositionsRec = FieldPosition::where('id', '=', $userProfile->field_position)->first();
                        $fieldPositions = $fieldPositionsRec->description;
                        $profilePic = '/images/profile/' . $userProfile->photo;
                        $profileName="Profile";
                        $profileUrl="/profile";

                    }
                    $createdAt = date('Y-m-d', strtotime(Auth::user()->created_at));
                    //$createdAt=Auth::user()->created_at;
                    $getnews = News::where('new', '=', 1)->get();
                    $countNews = count($getnews);
                    $gameRec = Game::orderBy('id', 'desc')->take(15)->get();

                    $view->with(array('userMessage' => $userMessage,
                        'countMessage' => $countMessage,
                        'userNotification' => $userNotification,
                        'userNotificationCount' => $userNotificationCount,
                        'userEvents' => $userEvents,
                        'userEventsCount' => $userEventsCount,
                        'userProfile' => $userProfile,
                        'fieldPositions' => $fieldPositions,
                        'createdAt' => $createdAt,
                        'getnews' => $getnews,
                        'countNews' => $countNews,
                        'gameRec' => $gameRec,
                        'profilePic' => $profilePic,
                        'profileName'=>$profileName,
                        'profileUrl'=>$profileUrl
                    ));
                });
        view()->composer('layouts.fanlayout', function ($view) {

            $profilePic = '/images/profile/669468.jpg';
           // $fieldPositions = "";
           // $userMessage = Message::where('user_id', '=', (Auth::user()->id))->get();
          //  $countMessage = count($userMessage);
            $userNotification = Notification::where('user_id', '=', (Auth::user()->id))->where('opened', '=', 0)->get();
            $userNotificationCount = count($userNotification);
            $date = date('Y-m-d H:i:s');
            $userEvents = Event::where('date', '>=', $date)->get();
            $userEventsCount = count($userEvents);
            $userProfile = Profile::where('user_id', '=', (Auth::user()->id))->first();
            //  dd($profilePic);
           /* if (count($userProfile) > 0) {
                $fieldPositionsRec = FieldPosition::where('id', '=', $userProfile->field_position)->first();
                $fieldPositions = $fieldPositionsRec->description;
                $profilePic = '/images/profile/' . $userProfile->photo;

            }*/
            $createdAt = date('Y-m-d', strtotime(Auth::user()->created_at));
            //$createdAt=Auth::user()->created_at;
            $getnews = News::where('new', '=', 1)->get();
            $countNews = count($getnews);
            $gameRec = Game::orderBy('id', 'desc')->take(15)->get();

            $view->with(array(/*'userMessage' => $userMessage,
                'countMessage' => $countMessage,*/
                'userNotification' => $userNotification,
                'userNotificationCount' => $userNotificationCount,
                'userEvents' => $userEvents,
                'userEventsCount' => $userEventsCount,
                'userProfile' => $userProfile,
                //'fieldPositions' => $fieldPositions,
                'createdAt' => $createdAt,
                'getnews' => $getnews,
                'countNews' => $countNews,
                'gameRec' => $gameRec,
                'profilePic' => $profilePic
            ));
        });

	}

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{

		$this->app->bind(
			'Illuminate\Contracts\Auth\Registrar',
			'App\Services\Registrar'
		);
	}

}
