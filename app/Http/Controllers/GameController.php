<?php namespace App\Http\Controllers;
use App\Model\News;
use App\Model\Game;
use App\Model\FieldPosition;
use App\Model\Country;
use App\Model\Profile;
use Request;
use Auth;



class GameController extends Controller {
    public $layout='layouts.default';


    public function post()
    {
        $layout='layouts.default';
        $title='Post Games';
        if (Request::has('team'))
        {
            Game::create(array(
                'game'=>Request::get('team'),
                'venue'=>Request::get('venue'),
                'date'=>Request::get('date'),
                'reference'=>Request::get('reference'),
                'opponent'=>Request::get('oponent'),
                'time'=>Request::get('time')
            ));
            return view('admin.games.index', array(
                'title'=>$title,
                'layout'=>$layout
            ));

        }
        return view('admin.games.index',
            array(
                'title'=>$title,
                'layout'=>$layout
            ));

    }


}
