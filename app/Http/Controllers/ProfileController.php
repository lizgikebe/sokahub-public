<?php namespace App\Http\Controllers;
use App\Model\News;
use App\Model\Game;
use App\Model\FieldPosition;
use App\Model\Country;
use App\Model\Profile;
use Request;
use Auth;



class ProfileController extends Controller {
    public $layout='layouts.default';

    public function index()
    {
        $title=Auth::user()->name." ".'Profile';
        $userProfile = Profile::where('user_id', '=', (Auth::user()->id))->first();
        $field_position=FieldPosition::where('id','=',$userProfile->field_position)->pluck('description');
        $country=Country::where('id','=',$userProfile->country_id)->first();
        $nationality=Country::where('id','=',$userProfile->nationality_id)->first();
        $profilePic='/images/profile/'.$userProfile->photo;
        return view('userprofile.index',
            array(
                'title'=>$title,
                'profilePic'=>$profilePic,
                'userProfile'=>$userProfile,
                'field_position'=>$field_position,
                'country'=>$country,
                'nationality'=>$nationality
            ));

    }
    public function create()
    {

        $fieldPosition=FieldPosition::all();
        $country=Country::all();
        $title='Create Profile Before Proceeding';

        if (Request::has('first_name'))
        {
            $destination_filename=Auth::user()->name;
            if (Request::hasfile('photo')) {
            $destinationPath = public_path() . '/images/profile/'; // upload path
            $extension = Request::file('photo')->getClientOriginalExtension(); // getting image extension
            $fileName = Auth::user()->id.rand(11111, 99999) . '.' . $extension; // renameing image
            Request::file('photo')->move($destinationPath, $fileName); // uploading file to given path
            $filePath = $destinationPath . $fileName;
        }

            $title="Profile Created";
            $userProfile=Request::all();
            Profile::create(array(
                'user_id'=>Auth::user()->id,
                'first_name'=>Request::get('first_name'),
                'other_names'=>Request::get('last_name'),
                'field_position'=>Request::get('field_position'),
                'leg'=>Request::get('leg'),
                'video_link'=>Request::get('video_link'),
                'current_club'=>Request::get('current_club'),
                'phone_no'=>Request::get('phone_no'),
                'description'=>Request::get('description'),
                'date_of_birth'=>Request::get('dob'),
                'gender'=>Request::get('gender'),
                'country_id'=>Request::get('country'),
                'nationality_id'=>Request::get('nationality'),
                'photo'=>$fileName
            ));
            return view('home', array('title'=>$title));
        }
        return view('userprofile.create-profile', array(
            'title'=>$title,
            'fieldPosition'=>$fieldPosition,
            'country'=>$country
        ));
    }
    public function edit()
    {
        $profileRec=Profile::where('user_id','=',Auth::user()->id)->first();

        return view('edit-profile', array('profileRec'=>$profileRec));

    }

}
