<?php namespace App\Http\Controllers;
use App\model\news;
use App\model\game;
use App\model\FieldPosition;
use App\model\Country;
use App\model\Profile;
use Request;
use Auth;



class ClientController extends Controller {

    public function index()
    {
            $layout='layouts.clientlayout';

        $title='Latest News ';
        $user_id=Auth::User()->id;
        $user_profile=Profile::where('user_id','=',$user_id)->first();
        $newsRec=News::all();
        return view('client.index',
            array(
                'title'=>$title,
                'newsRec'=>$newsRec,
                'layout'=>$layout
            ));

    }
    public function create()
    {

    }


}
