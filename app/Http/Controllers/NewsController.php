<?php namespace App\Http\Controllers;
use App\model\news;
use App\model\game;
use App\model\FieldPosition;
use App\model\Country;
use App\model\Profile;
use App\model\NewsType;
use Request;
use Auth;



class NewsController extends Controller {
    public $layout='layouts.default';

    public function index()
    {
        if(Auth::user()->role==1)
        {
            $layout='layouts.default';
            $title='Latest News ';
            $user_id=Auth::User()->id;
            $user_profile=Profile::where('user_id','=',$user_id)->first();
            $newsRec=News::all();
            return view('admin.news.index',
                array(
                    'title'=>$title,
                    'newsRec'=>$newsRec,
                    'layout'=>$layout
                ));
        }
        else
        {

            if(Auth::user()->role==4)
            {
                $layout='layouts.fanlayout';
            }

            if(Auth::user()->role==3)
            {
                $layout='layouts.default';
            }
            $title='Latest News ';
            $user_id=Auth::User()->id;
            $user_profile=Profile::where('user_id','=',$user_id)->first();
            $newsRec=News::all()->take(20);
            return view('users.news.index',
                array(
                    'title'=>$title,
                    'newsRec'=>$newsRec,
                    'layout'=>$layout
                ));
        }

    }
    public function view()
    {
        $layout='layouts.default';
        $id = Request::segment(2);
        $user_id=Auth::User()->id;
        $user_profile=Profile::where('user_id','=',$user_id)->first();
        $newsRec=News::where('id','=',$id)->first();
        $title='view News';
        return view('users.news.view',
            array(
                'title'=>$title,
                'newsRec'=>$newsRec,
                'layout'=>$layout,
                'id'=>$id
            ));

    }
    public function post()
    {
        $layout='layouts.default';
        $id = Request::segment(2);
        $user_id=Auth::User()->id;
        $news_type=NewsType::all();
        $user_profile=Profile::where('user_id','=',$user_id)->first();
        $newsRec=News::all()->take(10);
        $fileName="";
        $title='Post News';
        if (Request::has('news'))
        {
            if (Request::hasfile('photo')) {
                $destinationPath = public_path() . '/images/profile/'; // upload path
                $extension = Request::file('photo')->getClientOriginalExtension(); // getting image extension
                $fileName = Auth::user()->id.rand(11111, 99999) . '.' . $extension; // renameing image
                Request::file('photo')->move($destinationPath, $fileName); // uploading file to given path
                $filePath = $destinationPath . $fileName;
            }
            News::create(array(
                'news'=>Request::get('news'),
                'type'=>Request::get('type'),
                'title'=>Request::get('title'),
                'new'=>1,
                'image_url'=>$fileName
            ));
            return view('admin.news.index', array(
                'title'=>$title,
                'layout'=>$layout,
                'news_type'=>$news_type,
                'fileName'=>$fileName
            ));

        }
        return view('admin.news.index',
            array(
                'title'=>$title,
                'newsRec'=>$newsRec,
                'layout'=>$layout,
                'news_type'=>$news_type,
                'id'=>$id
            ));

    }


}
