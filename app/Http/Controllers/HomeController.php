<?php namespace App\Http\Controllers;
use App\model\news;
use App\model\game;
use Request;
use Auth;


class HomeController extends Controller {

    public function index($id=null)
    {
        if(Auth::user()->role==4)
        {
            $layout='layouts.fanlayout';
        }

    if(Auth::user()->role==3)
    {
        $layout='layouts.default';
    }
        $layout='layouts.default';
        $news=News::orderBy ( 'id', 'desc' )->take ( 15 )->get ();

        $title='Latest Soccer News';
         return view('home', array('title'=>$title,
             'newsRec'=>$news,
             'layout'=>$layout
         ))->with('id', $id);

    }
    public function activate()
    {

        return view('activate');

    }

}
