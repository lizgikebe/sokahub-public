<?php namespace App\Http\Controllers;
use App\model\news;
use App\model\Message;
use App\model\FieldPosition;
use App\model\Country;
use App\model\Profile;
use Request;
use Auth;



class MessageController extends Controller {
    public $layout='layouts.default';

    public function index()
    {
        $title='Messages ';
        $user_id=Auth::User()->id;
        $user_profile=Profile::where('user_id','=',$user_id)->first();
        $Messages=Message::where('user_id','=',Auth::User()->id)->get();

        return view('users.messages.index',
            array(
                'title'=>$title,
                'Messages'=>$Messages
            ));

    }
    public function view()
    {
        $id = Request::segment(2);
        $children=array();
        $messagesRec=Message::where('user_id','=',Auth::User()->id)->where('id','=',$id)->first();
        $messageDetails=Profile::where('user_id','=',$messagesRec->to_from)->first();


        if($messagesRec->is_parent==1)
        {
            $children=Message::where('parent_id','=',$messagesRec->id)->get();
        }
        $title='Messages';
        return view('users.messages.view',
            array(
                'messagesRec'=>$messagesRec,
                'id'=>$id,
                'title'=>$title,
                'children'=>$children,
                'messageDetails'=>$messageDetails
            ));

    }



}
