<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(array('before' => 'auth'), function() {
    Route::get('welcome', 'WelcomeController@index');
});

Route::group(array('middleware' => 'auth'), function()
{
    Route::get('/',
        [
            'as' => 'home',
            'uses' => 'HomeController@index'
        ]);


Route::get('/profile',
    [
        'as'=>'profile',
        'uses'=>'ProfileController@index'
    ]);
Route::get('/create-profile',
    [
        'as'=>'create-profile',
        'uses'=>'ProfileController@create'
    ]);
Route::post('/create-profile',
    [
        'as'=>'create-profile',
        'uses'=>'ProfileController@create'
    ]);

    Route::get('/latest-news',
        [
            'as'=>'latest-news',
            'uses'=>'NewsController@Index'
        ]);
    Route::get('/messages',
        [
            'as'=>'messages',
            'uses'=>'MessageController@Index'
        ]);
    Route::get('/message/{id?}',
        [
            function($id)
            {
                return $id;

            },
            'uses'=>'MessageController@view'
        ]

    );
    Route::get('client/dashboard',
        [
            'as'=>'dashboard',
            'uses'=>'ClientController@Index'
        ]);
    Route::get('/activate-account',
        [
            'as'=>'activate-account',
            'uses'=>'HomeController@Activate'
        ]);
    Route::get('/read-news/{id}',
        [
            'as'=>'read-news',
            'uses'=>'NewsController@View'
        ]);

    //Admin start
    Route::get('/view-news',
        [
            'as'=>'view-news',
            'uses'=>'NewsController@Post'
        ]);
    Route::post('/post-news',
        [
            'as'=>'post-news',
            'uses'=>'NewsController@Post'
        ]);
    Route::get('/view-games',
        [
            'as'=>'view-games',
            'uses'=>'GameController@Post'
        ]);
    Route::post('/post-games',
        [
            'as'=>'post-games',
            'uses'=>'GameController@Post'
        ]);
    //admin end
});

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
