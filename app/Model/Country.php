<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Country extends Eloquent {
    protected $table = "nationality";

}
