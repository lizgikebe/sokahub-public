<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Role extends Eloquent {
    protected $table = "roles";

}
