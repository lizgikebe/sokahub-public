<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model as Eloquent;

class News extends Eloquent {
    protected $table = "news";
    protected $fillable=[
        'news', 'type', 'image_url', 'title', 'new'

    ];
}
