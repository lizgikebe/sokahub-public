<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Notification extends Eloquent {
    protected $table = "notifications";

}
