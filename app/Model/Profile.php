<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Profile extends Eloquent {
    protected $table = "profile";
    protected $fillable=[
        'user_id','user_id','first_name', 'other_names',
        'field_position', 'leg', 'video_link', 'current_club'
        , 'phone_no', 'description', 'date_of_birth', 'gender',
        'country_id', 'nationality_id','photo'
    ];



}
