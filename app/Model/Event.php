<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Event extends Eloquent {
    protected $table = "events";

}
