<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model as Eloquent;

class NewsType extends Eloquent {
    protected $table = "news_types";

}
