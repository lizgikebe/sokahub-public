<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model as Eloquent;


class Message extends Eloquent {
    protected $table = "messages";
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function from()
    {
        return $this->belongsTo('App\User','to_from');
    }

}
