<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Game extends Eloquent {
    protected $table = "games";
    protected $fillable=[
       'game',
        'venue',
        'date',
        'reference',
        'opponent',
        'time'

    ];
}
