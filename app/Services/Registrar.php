<?php namespace App\Services;

use App\User;
use Validator;
use Illuminate\Contracts\Auth\Registrar as RegistrarContract;
use Request;

class Registrar implements RegistrarContract {

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	public function validator(array $data)
	{
		return Validator::make($data, [
            'role' => 'required|max:255',
			'name' => 'required|max:255',
			'email' => 'required|email|max:255|unique:users',
			'password' => 'required|confirmed|min:6',
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	public function create(array $data)
	{
        $result = "";
        $chars = "abcdefghijklmnopqrstuvwxyz*@#%^&_?!-0123456789";
        $charArray = str_split($chars);
        for($i = 0; $i < 150; $i++){
            $randItem = array_rand($charArray);
            $result .= "".$charArray[$randItem];
        }
        $randstr=$result;
		return User::create([
			'name' => $data['name'],
			'email' => $data['email'],
            'role' => $data['role'],
            'activation_token'=>$randstr,
			'password' => bcrypt($data['password']),
		]);



    }

}
